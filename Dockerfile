FROM python:3.6.8
ADD * ./
RUN mkdir /tmp/mnist_convnet_model
RUN pip install -r requirements.txt
CMD ['python', './cnnTutorial.py']